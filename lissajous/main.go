package main

import (
	"image"
	"image/color"
	"image/gif"
	"io"
	"math"
	"os"
)

var (
	palette = []color.Color{color.White, color.Black}
)

const (
	white_index = 0
	black_index = 1
)

func main() {
	lissajous(os.Stdout)
}

func lissajous(out io.Writer) {
	const (
		frames = 64
		res    = 0.001
		cycles = 10.0
		freq_y = 1.2
		size   = 100
		delay = 8
		amplitude_y = 1.0
	)
	var (
		phase = 0.0
	)
	anim := gif.GIF{LoopCount: frames}
	for f := 0; f < frames; f++ {
		//esto va a ser un frame
		rect := image.Rect(0, 0, 2*size+1, 2*size+1)
		img := image.NewPaletted(rect, palette)
		for t := 0.0; t < 2*math.Pi*cycles; t += res {
			x := math.Sin(t)
			y := amplitude_y*math.Sin(t*freq_y + phase)
			img.SetColorIndex(size+int(x*size), size+int(y*size), black_index)
		}
		phase += 0.1
		anim.Delay = append(anim.Delay,delay)
		anim.Image = append(anim.Image,img)

	}
	gif.EncodeAll(out, &anim)
}
