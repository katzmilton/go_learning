package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	var s = strings.Join(os.Args, " ")
	fmt.Println(s)
}
